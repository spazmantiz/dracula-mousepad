# Dracula for [Mousepad](https://salsa.debian.org/xfce-team/apps/mousepad)

> A dark theme for [Mousepad](https://github.com/jwilm/alacritty).

![Screenshot](./screenshot.png)

## Install

All instructions can be found at [draculatheme.com/mousepad](https://draculatheme.com/mousepad).

## License

[MIT License](./LICENSE)
